<?php

namespace App\Parser;

use App\Entity\News;
use DateTime;
use Exception;
use RuntimeException;
use Symfony\Component\DomCrawler\Crawler;

class RbcParser
{
    /**
     * Load urls of news from left block.
     */
    public static function getMainPageNews(string $source): array
    {
        $c = new Crawler($source);

        return $c->filter('.js-news-feed-list > a')->extract(['href']);
    }

    /**
     * get News entity based on data from html.
     *
     * @throws Exception
     */
    public static function getNewsData(string $source): News
    {
        $c = new Crawler($source);

        $news = new News();
        $news->setAuthors($c->filter('[itemprop="author"][itemtype="https://schema.org/Person"]')
            ->extract(['content']));

        $news->setImage($c->filter('link[itemprop="image"]')->link()->getUri());

        // TODO make filter smarter
        $body = $c->filter('[itemprop="articleBody"] p')->extract(['_text']);

        if (count($body)) {
            $body = '<p>'.implode("</p>\n<p>", $body).'</p>';
            $news->setBody($body);
        }

        $news->setTitle($c->filter('[itemprop="headline"]')->extract(['_text'])[0]);

        $canonical = $c->filter('link[rel="canonical"]')->link()->getUri();
        $tmp = explode('/', $canonical);

        if (!$tmp) {
            throw new RuntimeException('Unable to get id hash from '.$canonical);
        }

        $news->setSourceId(end($tmp));
        $news->setSourceUrl($canonical);
        $news->setSourcePublished(new DateTime($c->filter('[itemprop="datePublished"]')->extract(['content'])[0]));

        return $news;
    }
}
