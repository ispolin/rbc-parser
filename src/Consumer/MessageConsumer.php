<?php

namespace App\Consumer;

use App\Manager\NewsManager;
use App\Parser\RbcParser;
use Exception;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;

class MessageConsumer implements ConsumerInterface
{
    private NewsManager $newsManager;

    private LoggerInterface $logger;

    /**
     * MessageConsumer constructor.
     */
    public function __construct(NewsManager $newsManager, LoggerInterface $logger)
    {
        $this->newsManager = $newsManager;
        $this->logger = $logger;
    }

    public function execute(AMQPMessage $msg)
    {
        $message = json_decode($msg->body, true, 512, JSON_THROW_ON_ERROR);
        $this->logger->info($message['url']);

        $client = HttpClient::create();
        $response = $client->request('GET', $message['url']);

        // TODO Initialise proper parser here in case of one more source
        try {
            $news = RbcParser::getNewsData($response->getContent());
            $this->newsManager->create($news);
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }
}
