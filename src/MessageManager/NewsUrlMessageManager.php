<?php

namespace App\MessageManager;

use App\Producer\MessagingProducer;

class NewsUrlMessageManager
{
    private MessagingProducer $messagingProducer;

    public function __construct(MessagingProducer $messagingProducer)
    {
        $this->messagingProducer = $messagingProducer;
    }

    public function publishUrls(array $list): void
    {
        foreach ($list as $l) {
            $message = json_encode([
                'url' => $l,
            ], JSON_THROW_ON_ERROR);

            $this->messagingProducer->publish($message);
        }
    }
}
