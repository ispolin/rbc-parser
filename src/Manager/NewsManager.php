<?php

namespace App\Manager;

use App\Entity\News;
use Doctrine\Common\Persistence\ManagerRegistry;

class NewsManager
{
    private string $class = News::class;

    private ManagerRegistry $managerRegistry;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    public function create(News $news): void
    {
        $entityManager = $this->managerRegistry->getManagerForClass($this->class);
        $exist = $entityManager->getRepository($this->class)->findOneBy(['sourceId' => $news->getSourceId()]);

        if (!$exist) {
            $entityManager->persist($news);
            $entityManager->flush();
        }
    }
}
