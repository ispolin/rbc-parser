<?php

namespace App\Command;

use App\MessageManager\NewsUrlMessageManager;
use App\Parser\RbcParser;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;

class PublishTasksRbcCommand extends Command
{
    protected static $defaultName = 'app:publish-tasks:rbc';

    private NewsUrlMessageManager $messageManager;

    private LoggerInterface $logger;

    /**
     * PublishTasksRbkCommand constructor.
     */
    public function __construct(NewsUrlMessageManager $messageManager, LoggerInterface $logger)
    {
        parent::__construct();
        $this->messageManager = $messageManager;
        $this->logger = $logger;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Publish news from RBK main page to RabbitMQ to be parsed in future');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $client = HttpClient::create();
        $response = $client->request('GET', 'https://rbc.ru');

        $list = RbcParser::getMainPageNews($response->getContent());
        $this->logger->notice('Publish {cnt} urls', ['cnt' => count($list)]);
        $this->messageManager->publishUrls($list);

        $this->logger->notice('Done');

        return 0;
    }
}
