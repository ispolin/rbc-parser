<?php

namespace App\Command\Debug;

use App\Parser\RbcParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;

// TODO make it visible only for dev env
class RbcParserDebugCommand extends Command
{
    protected static $defaultName = 'app:debug:rbc-parser';

    protected function configure(): void
    {
        $this
            ->addOption('url', null, InputOption::VALUE_REQUIRED, 'News page')
            ->setDescription('Debug RBC parser');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $input->getOption('url'));
        dump(RbcParser::getNewsData($response->getContent()));

        return 0;
    }
}
