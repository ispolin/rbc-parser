FROM php:7.4-fpm-alpine as base
WORKDIR /app

RUN docker-php-ext-install mysqli pdo pdo_mysql

RUN apk update && apk add mysql-client \
     git \
     curl \
     unzip \
     tzdata \
     mc \
     wget \
     bash \
     sudo \
     openssh-client \
     shadow

RUN usermod -l app www-data -d /home/app -s /bin/bash -G wheel
RUN groupmod -n app www-data
RUN rm -rf /home/www-data
RUN chown -R app:app /app

USER app
